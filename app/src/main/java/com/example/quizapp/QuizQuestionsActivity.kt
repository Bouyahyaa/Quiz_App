package com.example.quizapp

import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.core.content.ContextCompat

class QuizQuestionsActivity : AppCompatActivity() , View.OnClickListener {

    private var mCurrentPosition : Int = 1
    private var mQuestionsList : ArrayList<Question>? = null
    private var mSelectedOptionPosition : Int = 0
    private var mCorrectAnswers : Int = 0
    private var mUserName : String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz_questions)
        val tv_option_one : TextView = findViewById(R.id.tv_option_one)
        val tv_option_Two : TextView = findViewById(R.id.tv_option_Two)
        val tv_option_Three : TextView = findViewById(R.id.tv_option_Three)
        val tv_option_Four : TextView = findViewById(R.id.tv_option_Four)
        val btn_submit : Button = findViewById(R.id.btn_submit)
        mUserName = intent.getStringExtra(Constants.USER_NAME)
        mQuestionsList = Constants.getQuestions()
        setQuestion()
        tv_option_one.setOnClickListener(this)
        tv_option_Two.setOnClickListener(this)
        tv_option_Three.setOnClickListener(this)
        tv_option_Four.setOnClickListener(this)
        btn_submit.setOnClickListener(this)
    }

    private fun setQuestion(){
        val progressBar : ProgressBar = findViewById(R.id.progressBar)
        val tv_progress : TextView = findViewById(R.id.tv_progress)
        val tv_question : TextView = findViewById(R.id.tv_question)
        val iv_image : ImageView = findViewById(R.id.iv_image)
        val tv_option_one : TextView = findViewById(R.id.tv_option_one)
        val tv_option_Two : TextView = findViewById(R.id.tv_option_Two)
        val tv_option_Three : TextView = findViewById(R.id.tv_option_Three)
        val tv_option_Four : TextView = findViewById(R.id.tv_option_Four)
        val btn_submit : Button = findViewById(R.id.btn_submit)
        val question = mQuestionsList!![mCurrentPosition-1]
        defaultOptionsView()

        if(mCurrentPosition == mQuestionsList!!.size){
            btn_submit.text = "FINISH"
        }else{
            btn_submit.text = "SUBMIT"
        }

        progressBar.progress = mCurrentPosition
        tv_progress.text = "${mCurrentPosition}" + "/" + progressBar.max
        tv_question.text = question!!.question
        iv_image.setImageResource(question.image)
        tv_option_one.text = question.optionOne
        tv_option_Two.text = question.optionTwo
        tv_option_Three.text = question.optionThree
        tv_option_Four.text = question.optionFour
    }

    private fun defaultOptionsView(){
        val options = ArrayList<TextView>()
        val tv_option_one : TextView = findViewById(R.id.tv_option_one)
        val tv_option_Two : TextView = findViewById(R.id.tv_option_Two)
        val tv_option_Three : TextView = findViewById(R.id.tv_option_Three)
        val tv_option_Four : TextView = findViewById(R.id.tv_option_Four)

        options.add(0,tv_option_one)
        options.add(1,tv_option_Two)
        options.add(2,tv_option_Three)
        options.add(3,tv_option_Four)

        for(option in options){
            option.setTextColor(Color.parseColor("#7A8089"))
            option.typeface = Typeface.DEFAULT
            option.background = ContextCompat.getDrawable(this,R.drawable.default_option_border_bg)

        }


    }

    override fun onClick(v: View?) {
        val tv_option_one : TextView = findViewById(R.id.tv_option_one)
        val tv_option_Two : TextView = findViewById(R.id.tv_option_Two)
        val tv_option_Three : TextView = findViewById(R.id.tv_option_Three)
        val tv_option_Four : TextView = findViewById(R.id.tv_option_Four)
        val btn_submit : Button = findViewById(R.id.btn_submit)

        when(v?.id){
            R.id.tv_option_one->{
                selectedOptionView(tv_option_one , 1)
            }
            R.id.tv_option_Two->{
                selectedOptionView(tv_option_Two , 2)
            }
            R.id.tv_option_Three->{
                selectedOptionView(tv_option_Three , 3)
            }
            R.id.tv_option_Four->{
                selectedOptionView(tv_option_Four , 4)
            }
            R.id.btn_submit ->{
                if(mSelectedOptionPosition == 0 ){
                    mCurrentPosition++

                    when{
                        mCurrentPosition <= mQuestionsList!!.size ->{
                            setQuestion()
                        }else ->{
                            val intent = Intent(this, ResultActivity::class.java)
                            intent.putExtra(Constants.USER_NAME,mUserName)
                            intent.putExtra(Constants.CORRECT_ANSWERS,mCorrectAnswers)
                            intent.putExtra(Constants.TOTAL_QUESTIONS,mQuestionsList!!.size)
                            startActivity(intent)
                            finish()
                        }
                    }

                }else{
                    val question = mQuestionsList?.get(mCurrentPosition - 1)
                    if(question!!.correctAnswer != mSelectedOptionPosition){
                        answerView(mSelectedOptionPosition,R.drawable.wrong_option_border_bg)
                    }else{
                        mCorrectAnswers++
                    }
                    answerView(question.correctAnswer,R.drawable.correct_option_border_bg)
                    if(mCurrentPosition == mQuestionsList!!.size){
                        btn_submit.text = "FINISH"
                    }else{
                        btn_submit.text = "GO TO NEXT QUESTION"
                    }
                    mSelectedOptionPosition = 0
                }
            }
        }
    }

    private fun answerView(answer : Int , drawableView : Int){
        val tv_option_one : TextView = findViewById(R.id.tv_option_one)
        val tv_option_Two : TextView = findViewById(R.id.tv_option_Two)
        val tv_option_Three : TextView = findViewById(R.id.tv_option_Three)
        val tv_option_Four : TextView = findViewById(R.id.tv_option_Four)

        when(answer){
            1->{
                tv_option_one.background = ContextCompat.getDrawable(this,drawableView)
            }
            2->{
                tv_option_Two.background = ContextCompat.getDrawable(this,drawableView)
            }
            3->{
                tv_option_Three.background = ContextCompat.getDrawable(this,drawableView)
            }
            4->{
                tv_option_Four.background = ContextCompat.getDrawable(this,drawableView)
            }
        }
    }

    private fun selectedOptionView(tv : TextView , selectedOptionNum : Int){
        defaultOptionsView()
        mSelectedOptionPosition = selectedOptionNum
        tv.setTextColor(Color.parseColor("#363A43"))
        tv.setTypeface(tv.typeface,Typeface.BOLD)
        tv.background = ContextCompat.getDrawable(this,R.drawable.select_option_border_bg)
    }

}